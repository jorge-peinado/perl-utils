####################################################################################################################
# Script for concatenating two column-like text files "horizontally". 
# Explanation:
#
#   It reads the first word (or line) of the first file,
#   write that word/line in the first line of the output file, 
#   write a tab (or the configured separator character) in the same line,
#   then the first word (or line) of the second file, and finally a new line character.
#   Then proceed with the second word of both files, third word, etc. etc.
#
# The origin of the script was the incapacity of some old versions of the Excel program 
# of managing files with more than 32768 rows, and the necessity of concatenating "horizontally" those files.
# The script can be used also for doing such concatenations outside of Excel in case the user has column-like 
# text files with a big number of lines.
#
# In case there are "holes" (some rows missing) in one of the files, a separator is inserted in place.
# If one of the files has more rows that the other, a separator is inserted in the missing cells 
# corresponding to the shorter file input.
#
# - param 1 (mandatory): name of the first file.
# - param 2 (mandatory): name of the second file.
#
# Author: Jorge Peinado
# Contact: jpp650@gmail.com
# Date: 2016.08.16
# Version: 2
####################################################################################################################

#
# Check number of parameters.
#

if (@ARGV != 2) {
  die "\nUsage: hconca2.pl <text-file1> <text-file2>\n\n";
}

#
# Variables
#

my $separator = "\t";
my $file1 = $ARGV[0];
my $file2 = $ARGV[1];
my $line1;
my $line2;
my $end1 = 0;
my $end2 = 0;

open(FILE1, "<", $file1) || die "Cannot open $file1.\n";
open(FILE2, "<", $file2) || die "Cannot open $file2.\n";
open(OUTFILE, ">", "hconcat2.out") || die "Cannot create hconcat2.out.\n";

#
# Check EOF condition at the very beginning
#

$end1 = eof(FILE1);
$end2 = eof(FILE2);

while (!($end1 and $end2)) {

  if (!$end1){
    $line1 = <FILE1>;
    chomp($line1);
    if (!$line1) {   
	  $line1 = "\t";
    }
	$end1 = eof(FILE1);
  } else { $line1 = "\t" ; }

  if (!$end2){
    $line2 = <FILE2>;
    chomp($line2);
    if (!$line2) {
      $line2 = $separator;
    }
	$end2 = eof(FILE2);
  } else { $line2 = $separator; }

  print OUTFILE "$line1$separator$line2\n";
}

