####################################################################################################################
# Script that implements the grep command for Windows.
#
# - param 1 (optional):  flag -v (negative matching) or -i (upper or lowercase matching), 
#                        or the compact version -vi or -iv (that does both kinds of matching).
# - param 2 (mandatory): string to look for. Stored in the global variable $pattern.
# - param 3 (mandatory): file or files to search into. Also accepts a Windows command prompt file mask.
#                        Stored in the global variable $filespec.
#
# Author: Jorge Peinado
# Contact: jpp650@gmail.com
# Date: 2016.08.15
# Version: 3
###################################################################################################################

my $negative = 0;
my $anycasematch = 0;
my $pattern;
my $filespec;

#
# Auxiliar variables. $dir contains the full output of the Windows command prompt dir command,
# and @filelist is an array containing one file name per cell.
#

my $dir;
my @filelist;


################################################################################
# Parameter checking function.
# It checks the parameters passed to the script.
################################################################################

sub check_parameters {

  #
  # Check number of arguments
  #

  if (@ARGV < 2 || @ARGV > 3) {
    die "\nUsage: grep.pl [-v|-i|-vi] <pattern> <file name | partial file name with *>\n\n";
  }
  
  #
  # If we have three arguments, check the first one
  #
    
  if (@ARGV == 3) {
    if ($ARGV[0] =~ /^-v$/) { 
	  $negative = 1; }
	elsif ($ARGV[0] =~ /^-i$/) { 
	  $anycasematch = 1; }
	elsif (($ARGV[0] =~ /^-vi$/) || ($ARGV[0] =~ /^-iv$/)) { 
	  $negative = 1; 
	  $anycasematch = 1; }
	else { 
	  die "\nUsage: grep.pl [-v|-i|-vi] <pattern> <file name | partial file name with *>\n\n"; }
  	$pattern = $ARGV[1];
	$filespec = $ARGV[2];
	
  } else {
  
    #
    # Otherwise we have two arguments
    #
  
  	$pattern = $ARGV[0];
	$filespec = $ARGV[1];
  }
}


################################################################################
# Main program.
################################################################################

check_parameters();

#
# Read the directory without details (/b flag).
# We want only file names. Split the list in the new line character.
#

$dir = `dir /b $filespec`;
if ($dir eq "") {
  die "\nNo matching file found.\n\n";
}
@filelist = split("\n", $dir);

#
# Uppercase and lowercase versions of the pattern,
# for doing a completely uppercase and completely lowercase comparision.
#

my $uc_pattern = uc($pattern);
my $lc_pattern = lc($pattern);
my $uc_line = "_";
my $lc_line = "_";

#
# If the search is "negative" we match everything except the pattern.
#

if ($negative == 1) {

  #
  # Negative matching.
  # Check the any case.
  # 

  if ($anycasematch == 1) {
  
    #
	# Negative matching. Any case matching.
	#
  
    for my $i (0 .. $#filelist) {
	
	  #
	  # Try to open the file.
	  #
	
	  if (open(FILE,"<", "$filelist[$i]")) {
	    while (my $line = <FILE>) {
          chomp($line);
	      $uc_line = uc($line);
	      $lc_line = lc($line);
	      if ($uc_line =~ /$uc_pattern/ || $lc_line =~ /$lc_pattern/) { next; }
	      else { print "$filelist[$i]:\t\t\t$line\n"; }
        }
	  } else {
	  
	    #
	    # Cannot open this file. Next one.
	    #
	  
	    print "\nCannot open file $filelist[$i]. Next one.\n";
		next;
	  }
	}
  } else {
  
    #
    # Negative matching. Exact matching.
    #
  
    for my $i (0 .. $#filelist) {	
	  if (open(FILE,"<", "$filelist[$i]")) {
        while (my $line = <FILE>) {
          chomp($line);	  
          if ($line =~ /$pattern/) { next; }
	      else { print "$filelist[$i]:\t\t\t$line\n"; }
        }
		
	  } else { 
	    print "\nCannot open file $filelist[$i]. Next one.\n";
	    next;
	  }
    }
  }
} else {

  #
  # Positive matching.
  # Check the any case.
  # 
  
  if ($anycasematch == 1) {
  
    #
	# Positive matching. Any case matching.
	#
	
	for my $i (0 .. $#filelist) {	
	  if (open(FILE,"<", "$filelist[$i]")) {
        while (my $line = <FILE>) {
          chomp($line);
	      $uc_line = uc($line);
	      $lc_line = lc($line);
	      if ($uc_line =~ /$uc_pattern/ || $lc_line =~ /$lc_pattern/) { print "$filelist[$i]:\t\t\t$line\n"; }
	      else { next; }
        }
	  } else {
	  	print "\nCannot open file $filelist[$i]. Next one.\n";
	    next;
	  }
	}
  } else {
  
    #
    # Positive matching. Exact matching.
    #
	
	for my $i (0 .. $#filelist) {	
	  if (open(FILE,"<", "$filelist[$i]")) { 
	    while (my $line = <FILE>) {
          chomp($line);
          if ($line =~ /$pattern/) { print "$filelist[$i]:\t\t\t$line\n"; }
	      else { next; }
	    }
	  
	  } else {
	    print "\nCannot open file $filelist[$i]. Next one.\n";
	    next;  
	  }
    }
  }  
}
