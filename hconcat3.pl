####################################################################################################################
# Script for concatenating three column-like text files "horizontally". 
# Based on hconcat2.pl but with three files in place of two.
#
# - param 1 (mandatory): name of the first file.
# - param 2 (mandatory): name of the second file.
# - param 3 (mandatory): name of the third file.
#
# Author: Jorge Peinado
# Contact: jpp650@gmail.com
# Date: 2016.08.16
# Version: 2
####################################################################################################################

#
# Check number of parameters.
#

if (@ARGV != 3) {
  die "\nUsage: hconca3.pl <text-file1> <text-file2> <text-file3>\n\n";
}

#
# Variables
#

my $separator = "\t";
my $file1 = $ARGV[0];
my $file2 = $ARGV[1];
my $file3 = $ARGV[2];
my $line1;
my $line2;
my $line3;
my $end1 = 0;
my $end2 = 0;
my $end3 = 0;

open(FILE1, "<", $file1) || die "Cannot open $file1.\n";
open(FILE2, "<", $file2) || die "Cannot open $file2.\n";
open(FILE3, "<", $file3) || die "Cannot open $file3.\n";
open(OUTFILE, ">", "hconcat3.out") || die "Cannot create hconcat3.out.\n";

#
# Check EOF condition at the very beginning
#

$end1 = eof(FILE1);
$end2 = eof(FILE2);
$end2 = eof(FILE3);

while (!($end1 and $end2 and $end3)) {

  if (!$end1){
    $line1 = <FILE1>;
    chomp($line1);
    if (!$line1) {   
	  $line1 = "\t";
    }
	$end1 = eof(FILE1);
  } else { $line1 = "\t" ; }

  if (!$end2){
    $line2 = <FILE2>;
    chomp($line2);
    if (!$line2) {
      $line2 = $separator;
    }
	$end2 = eof(FILE2);
  } else { $line2 = $separator; }
  
  if (!$end3){
    $line3 = <FILE3>;
    chomp($line3);
    if (!$line3) {
      $line3 = $separator;
    }
	$end3 = eof(FILE3);
  } else { $line3 = $separator; }

  print OUTFILE "$line1$separator$line2$separator$line3\n";
}

